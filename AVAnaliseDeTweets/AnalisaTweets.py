#!/usr/bin/python3 -u

from __future__ import absolute_import, print_function
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
import json
import re
import time

''' Objetivo
Dadas as palavras chave, encontrar:
    * os principais canais de divulgação de dados (utilizando as URLS)
    * os principais influenciadores (pelos RTs)
    * as principais HashTags
'''

start = time.time()

influenciadores = {}
hashtags        = {}
canais          = {}

class AssinanteTwitter(StreamListener):
    # Essa função será invocada automaticamente toda vez que um twitter for identificado
    def on_data(self, data):
        conteudoJSON = json.loads(data)
        try:
            text = conteudoJSON['text']
            match = re.match('RT @(.*?):\s', text)
            if match:
                influenciador = match.group(1)
                print("Found influenciador: " + influenciador)
                if influenciador in influenciadores:
                    influenciadores[influenciador] += 1
                else:
                    influenciadores[influenciador] = 1

            for hashtag_info in conteudoJSON['entities']['hashtags']:
                hashtag = hashtag_info['text']
                print("Found hashtag: " + hashtag)
                if hashtag in hashtags:
                    hashtags[hashtag] += 1
                else:
                    hashtags[hashtag] = 1

            for url_info in conteudoJSON['entities']['urls']:
                url = url_info['expanded_url']
                match = re.match('https?://(.*?)/', url)
                if match:
                    canal = match.group(1)
                    print("Found canal: " + canal)
                    if canal in canais:
                        canais[canal] += 1
                    else:
                        canais[canal] = 1
        except:
            return True

        elapsed = time.time() - start
        if elapsed > 600:
            print("Canais:")
            for canal in canais:
                print("{}\t{}".format(canal, canais[canal]))

            print("Influenciadores:")                
            for influenciador in influenciadores:
                print("{}\t{}".format(influenciador, influenciadores[influenciador]))

            print("Hashtags:")    
            for hashtag in hashtags:
                print("{}\t{}".format(hashtag, hashtags[hashtag]))

            return False # Encerra o processo

        return True

    # Essa função será invocada automaticamente toda vez que ocorrer um erro
    def on_error(self, status):
        print(status)

# Para executar esse exemplo é preciso possuir uma conta no twitter, caso não possua crie uma.
# Entre no site http://apps.twitter.com e crie uma nova applicação preenchendo as informações
# Será gerado o consumer key e o consumer secret, que são a identificação de sua aplicação no twitter.
# print("Inicio do programa")
consumer_key = "CTsMvNd1Jvwrq0k8HjfvJLPCq"
consumer_secret = "NP58yl7lWAROViW6US8KciUcK69G5VNt87Z8jsYbFfKl8P6Xn5"
# Você será redirecionado para outra página, clique na aba 'Keys and Access Tokens'
# Crie um token de acesso novo, ele será utilizado no lugar de suas credenciais
access_token = "240533001-oHDSvdKi6hEfNNwY6EzDmL5lPIsHI7v5T4uozEbM"
access_token_secret = "VVQFaAZLVnoDcgqyFVpOvRRjkYuGo1F1C7SFazEUwxKTj"
assinante = AssinanteTwitter()
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)
stream = Stream(auth, assinante)
stream.filter(track=['stf', 'crusoe', 'censura', 'impeachment', 'ditadura', 'lavatoga'], languages=["pt"])
